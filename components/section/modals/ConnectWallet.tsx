import { ConnectionStage, WalletsContext } from "../../context/waletContext";
import ModalLayout from "../../layout/ModalLayout";
import { IConnectWallet } from "../../interfaces";
import { useContext } from "react";
import Button from "../../button";




const ConnectWallet = ({ connectWallet }: IConnectWallet) => {
  const { handleWalletConnect, stage, errorMessage } = useContext(WalletsContext);


  return (
    <ModalLayout {...connectWallet}>
      <div
        className={`bg-gray-400 md:w-[568px] w-[343px] rounded-2xl p-8 font-roboto`}
      >
        <div className="flex justify-between mb-8 ">
          <span className="text-xl">Connect a wallet</span>
          <img
            src="/CloseIcon.svg"
            className="cursor-pointer w-5"
            onClick={(e) => {
              connectWallet.close();
            }}
          />
        </div>
        {
          stage === ConnectionStage.NOTCONNECTED &&
          <>
            <div
              className={`${"flex"
                } bg-blue-100 cursor-pointer px-4 py-5 rounded-lg justify-between items-center`}
              onClick={() => {
                handleWalletConnect();
              }}
            >
              <span className="text-xl">Meta Mask</span>
            </div>
          </>
        }

        {stage === ConnectionStage.PENDING &&
          (
            <div className={`flex flex-col pb-9 items-center`}>
              <p className="font-roboto text-xl pt-5">Connection...</p>
            </div>
          )}
        {stage === ConnectionStage.SUCCESS &&
          <div className="flex flex-col items-center">
            <p className="pt-2 pb-8">Wallet connected</p>
            <Button className='w-full py-10px rounded-xl' onClick={(e) => { connectWallet.close(); }}>Confirm</Button>
          </div>
        }
        {stage === ConnectionStage.ERROR && (
          <div className="flex flex-col items-center">
            <p className="pt-2 ">Failed to connect wallet</p>
            <span className="pt-2 mb-8"> {errorMessage}</span>
            <Button className='w-full py-10px rounded-xl' onClick={handleWalletConnect}>Try again</Button>
          </div>
        )}
      </div>
    </ModalLayout>
  );
};
export default ConnectWallet;
