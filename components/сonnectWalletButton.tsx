import { useContext, useEffect, useState } from "react"
import { useModal } from "../components/layout/ModalLayout"
import ConnectWallet from "./section/modals/ConnectWallet"
import { ConnectionStage, WalletsContext } from "./context/waletContext"
// import { truncate } from "fs";

const ConnectWalletButton = () => {
    const { defaultAccount, stage, setStage, handleWalletDisconnect } =
        useContext(WalletsContext);
    const [isToggled, setIsToggled] = useState(false);
    const connectWallet = useModal();

    return stage === ConnectionStage.SUCCESS ? (
        <>
            <div className={`${isToggled ? '' : ''} flex items-center cursor-pointer bg-gray-500 justify-center px-3 md:px-4 py-10px rounded-xl relative`}
                onClick={() => { setIsToggled(!isToggled) }}>
                <div className={`${isToggled ? 'visible' : 'invisible'} w-full bg-gray-200 flex items-center justify-between cursor-pointer p-4 rounded-xl absolute z-1`}
                    onClick={() => {
                        handleWalletDisconnect()
                        setIsToggled(false)
                    }}>
                    <p className="" >Disconnect</p >
                    <div className="stroke-current text-white">
                        Disconnect
                    </div>
                </div >
            </div>

        </>
    ) : (
        <>
            <ConnectWallet connectWallet={connectWallet} />
            <div
                className="flex items-center cursor-pointer bg-gray-500 w-fit justify-center px-4 py-1 rounded-xl"
                onClick={connectWallet.open}
            >
                <p className="text-base-m text-black">Connect Wallet</p>
            </div>
        </>

    );
};
export default ConnectWalletButton;

