export interface IModal {
  className?: string;
  isOpen: boolean;
  open: (arg0: string) => void;
  close: () => void;
  customClasses?: boolean;
  children: any;
  onSideClick?: () => void;
}

export interface IConnectWallet {
  connectWallet: any;
}

export interface Orders {
  id: string
  balance: number
  pricePerImage: number
  customer: string
  deadline: number
  verifier: string
  url: string
}