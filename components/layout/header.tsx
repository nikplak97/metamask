import { useRouter } from 'next/router';
import React, { useState, useEffect, useContext } from 'react';
import { ConnectContext } from '../context/connectWalletContext';

export default function Header() {
    const router = useRouter()
    const { logined } = useContext(ConnectContext)
    const { setLoginet } = useContext(ConnectContext)

    return (
        <>
            <div className={`w-full p-4`}>
                <div className={`flex flex-col items-center`}>
                    <button
                        onClick={() => {
                            //@ts-ignore
                            setLoginet(logined => !logined)
                            router.push('/project')
                        }}
                        disabled={logined}
                        className={`text-2xl font-bold p-4 bg-blue-600 rounded-full
                        
                        `}>
                        {logined ? '0xFcB…F860' : 'Connect Wallet'}
                    </button>
                </div>
            </div>
        </>
    )
}