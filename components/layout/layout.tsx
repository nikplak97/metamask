import React, { useState, useEffect, ReactNode, useContext } from 'react';
import { ConnectContext } from '../context/connectWalletContext';
import Header from './header';

type Props = {
    children?: ReactNode,
}

export default function Layout({ children }: Props) {

    return (
        <>
            <div className={`bg-cyan-200 min-h-screen`}>
                <Header />
                {children}
            </div>
        </>
    )
}