import React, { useState, useEffect } from 'react';
import { ProjectData } from './projectData';


export default function RandomImage({ images }: any) {
    const [sunrise, setSunrise] = useState(false)
    const [shine, setShine] = useState(false)
    const [cloudy, setCloudy] = useState(false)
    const [rain, setRain] = useState(false)
    const [sum, setSum] = useState(0)
    const prise = (sum * ProjectData[0].pricePerImage).toFixed(2)

    function arrayRandElement(arr: any) {
        var rand = Math.floor(Math.random() * arr.length);
        return arr[rand];
    }
    const [random, setRandom] = useState(arrayRandElement(images))

    return (
        <>
            <div className={`flex flex-col justify-center items-center text-xl`} >
                <div className={`w-full rounded-2xl bg-cyan-400 flex justify-between my-4 px-4 py-2`}>
                    <p>Total images:</p>
                    <p className={`font-bold text-cyan-800`}>{sum}</p>
                </div>
                <div className={`w-full rounded-2xl bg-cyan-400 flex justify-between my-4 px-4 py-2`}>
                    <p>You will earn:</p>
                    <p className={`font-bold text-cyan-800`}>{prise}{ProjectData[0].сurrency}</p>
                </div>
                <div>{random}</div>
                <div className={`flex space-x-1 mt-4`}>
                    <button
                        onClick={() => {
                            setSunrise(!sunrise)
                        }}
                        className={`rounded-xl focus:bg-blue-500 py-1 px-4 bg-blue-300 border border-blue-500 font-medium focus:font-bold`}
                    >Sunrise</button>
                    <button
                        onClick={() => {
                            setShine(!shine)
                        }}
                        className={`rounded-xl focus:bg-blue-500 py-1 px-4 bg-blue-300 border border-blue-500 font-medium focus:font-bold`}
                    >Shine</button>
                    <button
                        onClick={() => {
                            setCloudy(!cloudy)
                        }}
                        className={`rounded-xl focus:bg-blue-500 py-1 px-4 bg-blue-300 border border-blue-500 font-medium focus:font-bold`}
                    >Cloudy</button>
                    <button
                        onClick={() => {
                            setRain(!rain)
                        }}
                        className={`rounded-xl focus:bg-blue-500 py-1 px-4 bg-blue-300 border border-blue-500 font-medium focus:font-bold`}
                    >Rain</button>
                </div>
                <button
                    disabled={
                        !shine && !cloudy && !sunrise && !rain
                    }
                    onClick={() => {
                        setSum(sum + 1)
                        setRandom(arrayRandElement(images))
                        setSunrise(false)
                        setShine(false)
                        setCloudy(false)
                        setRain(false)
                    }}
                    className={`bg-blue-300 w-full text-center p-4 mt-4 active:bg-blue-600 rounded-xl`}>
                    Submit
                </button>
            </div>
        </>
    )
}