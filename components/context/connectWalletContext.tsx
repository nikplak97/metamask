import React, { useState, useEffect } from 'react'

interface Props {
    children: any
}

const ConnectContext = React.createContext({
    logined: false,
    setLoginet: (arg: boolean) => { },
})

const ConnectWallteContext = ({ children }: Props) => {
    const [logined, setLoginet] = useState(false)

    return (
        <ConnectContext.Provider
            value={{
                logined,
                setLoginet,
            }}
        >
            {children}
        </ConnectContext.Provider>
    )
}

export default ConnectWallteContext

export { ConnectContext }