import axios from "axios"
import { Web3 } from "../../core/web3/web3.provider"
import { ethers } from "ethers"
import React, { SetStateAction, useEffect, useLayoutEffect, useState } from "react"

interface Props {
    children: any;
}

export enum ConnectionStage {
    "NOTCONNECTED" = "Not connected",
    "PENDING" = "Pending",
    "ERROR" = "Error",
    "SUCCESS" = "Succes",
}

export const WalletsContext = React.createContext({
    errorMessage: "",
    setErrorMessage: (arg: any) => { },
    defaultAccount: "",
    setDefaultAccount: (arg: any) => { },
    userId: "",
    setUserId: (arg: any) => { },
    userBalance: "",
    setUserBalance: (arg: any) => { },
    stage: "",
    setStage: (arg: SetStateAction<ConnectionStage>) => { },
    handleWalletConnect: () => { },
    handleWalletDisconnect: () => { },
});

const WalletsContextProvider = ({ children }: Props) => {
    const [errorMessage, setErrorMessage] = useState("");
    const [defaultAccount, setDefaultAccount] = useState("");
    const [userId, setUserId] = useState("")
    const [userBalance, setUserBalance] = useState("");
    const [isFirstRender, setIsFirstRender] = useState(true);
    const [stage, setStage] = useState(ConnectionStage.NOTCONNECTED);

    useLayoutEffect(() => {
        !isFirstRender && localStorage.setItem("wallet", stage);
        setIsFirstRender(false);
    }, [stage]);

    // test connect wallet with if network
    const handleWalletConnect = () => {
        setStage(ConnectionStage.PENDING);
        if (window.ethereum && window.ethereum.isMetaMask) {
            window.ethereum
                .request({ method: "eth_chainId" })
                .then((network: string) => {
                    if (network === "0x38" && window.ethereum.networkVersion === "56") {
                        requestAccounts();
                    } else {
                        window.ethereum
                            .request({
                                method: "wallet_switchEthereumChain",
                                params: [{ chainId: "0x38" }],
                            })
                            .then(() => {
                                requestAccounts();
                            })
                            .catch((error: any) => {
                                if (error.code === 4902) {
                                    window.ethereum
                                        .request({
                                            method: "wallet_addEthereumChain",
                                            params: [
                                                {
                                                    chainId: "0x38",
                                                    chainName: "BSC Mainnet",
                                                    nativeCurrency: {
                                                        name: "BNB",
                                                        symbol: "bnb",
                                                        decimals: 18,
                                                    },
                                                    rpcUrls: ["https://bsc-dataseed1.ninicoin.io"],
                                                    blockExplorerUrls: ["https://bscscan.com/"],
                                                },
                                            ],
                                        })
                                        .then(() => {
                                            requestAccounts();
                                        })
                                        .catch((error: any) => {
                                            setErrorMessage(error.message);
                                            setStage(ConnectionStage.ERROR);
                                        });
                                } else {
                                    setErrorMessage(error.message);
                                    setStage(ConnectionStage.ERROR);
                                }
                            });
                    }
                })
                .catch((error: any) => {
                    setErrorMessage(error.message);
                    setStage(ConnectionStage.ERROR);
                });
        } else {
            setErrorMessage("Please install MetaMask browser extension to interact");
            setStage(ConnectionStage.ERROR);
        }
    };

    useLayoutEffect(() => {
        if (localStorage.getItem("wallet") === ConnectionStage.SUCCESS)
            handleWalletConnect();
    }, []);

    const requestAccounts = () => {
        window.ethereum
            .request({ method: "eth_requestAccounts" })
            .then((result: string) => {
                accountChangedHandler(result[0]);
                getAccountBalance(result[0]);
                setStage(ConnectionStage.SUCCESS);
            })
            .catch((error: any) => {
                setErrorMessage(error.message);
                setStage(ConnectionStage.ERROR);
            });
    };

    const accountChangedHandler = async (newAccount: string) => {
        setDefaultAccount(newAccount);
        const web3 = new Web3();
        web3.setDefaultAccount(newAccount);
        getAccountBalance(newAccount.toString());
    };

    const getAccountBalance = (account: string) => {
        window.ethereum
            .request({ method: "eth_getBalance", params: [account, "latest"] })
            .then((balance: string) => {
                setUserBalance(ethers.utils.formatEther(balance));
            })
            .catch((error: any) => {
                setErrorMessage(error.message);
            })
    }

    const handleWalletDisconnect = () => {
        window.ethereum
            .request({
                method: "eth_requestAccounts",
                params: [{ eth_accounts: {} }],
            })
            .then((result: string) => {
                accountChangedHandler("");
                getAccountBalance("");
                setStage(ConnectionStage.NOTCONNECTED);
            })
            .catch((error: any) => {
                setErrorMessage(error.message);
            });
    };


    return (
        <WalletsContext.Provider
            value={{
                errorMessage,
                setErrorMessage,
                defaultAccount,
                setDefaultAccount,
                userId,
                setUserId,
                userBalance,
                setUserBalance,
                handleWalletConnect,
                handleWalletDisconnect,
                stage,
                setStage,
            }}
        >
            {children}
        </WalletsContext.Provider>
    );
};
export default WalletsContextProvider;
