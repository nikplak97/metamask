
interface IButton {
    children: any,
    onClick?: (arg:any) => void,
    className?: string,
    active?: boolean
}

const Button = ({
    children,
    className,
    onClick,
    active = true
}: IButton) => {
    return (
        <button className={`${className} flex items-center justify-center bg-menu-light-blue ${active ? "hover:bg-button-hover-blue active:bg-button-active-blue" : 'opacity-40'}  cursor-pointer`}
            onClick={onClick}
            disabled = {!active}
            >
            {children}
        </button>
    )
}
export default Button