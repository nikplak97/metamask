import type { NextPage } from 'next'
import Layout from '../components/layout/layout'
import ConnectWalletButton from '../components/сonnectWalletButton'

const Home: NextPage = () => {
  return (
    <>
      <div className={`p-4`}>
        <p className={`text-5xl font-semibold`}>Welcome to DDAA! Your best way to earn crypto on the go.</p>
      </div>
    </>
  )
}

export default Home
