import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Layout from '../components/layout/layout'
import ConnectWallteContext from '../components/context/connectWalletContext'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ConnectWallteContext>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ConnectWallteContext>
  )
}

export default MyApp
