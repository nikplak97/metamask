import { useRouter } from 'next/router';
import React, { useState, useEffect, useContext } from 'react';
import { ConnectContext } from '../../components/context/connectWalletContext';
import { ProjectData } from '../../components/projectData';

export default function Project() {
    const router = useRouter()
    const { logined } = useContext(ConnectContext)
    const { setLoginet } = useContext(ConnectContext)

    return (
        <>
            <div className={`w-full h-full p-4`}>
                <div className={` mt-4 w-full h-full min-h-screen rounded-lg p-4`}>
                    <div className={`${logined ? 'flex' : 'hidden'} flex-col space-y-4`}>
                        {ProjectData.map((item, index) => (
                            <div key={index}
                                className={`rounded-3xl text-center p-4 bg-cyan-300 border-2 border-blue-500 text-lg flex flex-col px-10`}
                            >
                                <div className={`flex items-center justify-center w-full`}>
                                    <p className={`text-3xl font-semibold text-center`}>{item.customer}</p>
                                </div>
                                <div className={`flex justify-between items-center mt-1`}>
                                    <p>{item.description}</p>
                                </div>
                                <div>
                                    <p><span className={`font-extrabold mr-1`}>{item.pricePerImage}</span>{item.сurrency} per image</p>
                                </div>
                                <div className={`mt-4`}>
                                    <a
                                        onClick={() => { router.push(`/project/${item.url}`) }}
                                        // href={`/pages/${item.url}`}
                                        className={`py-1 px-4 bg-blue-600 rounded-full`}>Go</a>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </>
    )
}