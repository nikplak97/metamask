import { useRouter } from 'next/router'
import React, { useState, useEffect } from 'react'
import { ProjectData } from '../../../components/projectData'
import RandomImage from '../../../components/randomImage'

export default function ProjectPage() {
    const router = useRouter()
    const { id } = router.query
    const [content, setContent] = useState<Array<{
        id: string,
        balance: string,
        pricePerImage: string,
        сurrency: number,
        customer: string,
        deadline: string,
        verifier: string,
        url: string,
        description: string,
    }>>()


    useEffect(() => {
        //@ts-ignore
        setContent(ProjectData.filter(item => item.url === id))
    }, [])

    let images: JSX.Element[] = []
    for (let index = 1; index < 1100; index++) {
        images.push(<img className={`w-96 h-52 rounded-2xl`} key={index} src={`/dataset2/image (${index}).jpg`} />)
    }

    return (
        <>
            <div className={`p-4 flex flex-col justify-start bg-cyan-200 min-h-screen m-4`}>
                <RandomImage images={images} />
            </div>
        </>
    )
}
