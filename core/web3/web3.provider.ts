// const mixin = require("merge-descriptors");
import Web3js from "web3";

export class Web3 extends Web3js {
  static instance: Web3;
  static web3: Web3js;
  // static provider:
  constructor() {
    super();
    if (typeof Web3.instance === "object") {
      return Web3.instance;
    }
    Web3.instance = this;
    Web3.web3 = new Web3js(window.ethereum);
    Web3.web3.eth.setProvider(window.ethereum);
    return Web3.instance;
  }

  /**
   * initialization
   */
  public async initialization() {
    const account = await this.provider.getAccounts();
    if (account.length > 0) {
      this.provider.defaultAccount = account[0];
    }
  }

  /**
   * setDefaultAccount - set state account
   */
  public setDefaultAccount(address: string) {
    this.provider.defaultAccount = address;
  }

  public get provider() {
    return Web3.web3.eth;
  }

  public get fromAddress() {
    return this.provider.defaultAccount;
  }

  public async signByDefaultAddress(address: string, message: string): Promise<string> {
    return (await Web3.web3?.eth?.personal?.sign(Web3.web3.utils.utf8ToHex(message), address, 'sign')) as string;
  }

  /**
   * @returns - initialized class
   */
  public static get isInit(): boolean {
    return !!Web3.instance;
  }
}
